package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class Calculator extends JFrame {

	private final JTextField pol1Field;
	private final JTextField pol2Field;
	private final JTextField resPolField;
	private final JButton addButton;
	private final JButton subtractButton;
	private final JButton multButton;
	private final JButton divideButton;
	private final JButton modButton;
	private final JButton derivateButton;
	private final JButton integrateButton;

	public Calculator() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Polynomial Calculator");
		setBounds(100, 100, 277, 277);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("Polynomial Calculator");
		title.setFont(new Font("Tahoma", Font.PLAIN, 18));
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setBounds(33, 7, 193, 22);
		contentPane.add(title);
		
		JLabel label1 = new JLabel("First polynomial:");
		label1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		label1.setBounds(10, 40, 87, 14);
		contentPane.add(label1);
		
		JLabel label2 = new JLabel("Second polynomial:");
		label2.setFont(new Font("Tahoma", Font.PLAIN, 10));
		label2.setBounds(10, 65, 102, 14);
		contentPane.add(label2);
		
		pol1Field = new JTextField();
		pol1Field.setBounds(115, 37, 141, 20);
		contentPane.add(pol1Field);
		pol1Field.setColumns(10);
		
		pol2Field = new JTextField();
		pol2Field.setBounds(115, 62, 141, 20);
		contentPane.add(pol2Field);
		pol2Field.setColumns(10);
		
		JLabel label3 = new JLabel("Result polynomial:");
		label3.setFont(new Font("Tahoma", Font.PLAIN, 10));
		label3.setBounds(10, 90, 102, 14);
		contentPane.add(label3);
		
		resPolField = new JTextField();
		resPolField.setEditable(false);
		resPolField.setBounds(115, 87, 141, 20);
		contentPane.add(resPolField);
		resPolField.setColumns(10);
		
		addButton = new JButton("Add");
		addButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		addButton.setBounds(23, 115, 89, 23);
		contentPane.add(addButton);
		
		subtractButton = new JButton("Subtract");
		subtractButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		subtractButton.setBounds(148, 115, 89, 23);
		contentPane.add(subtractButton);
		
		multButton = new JButton("Multiplicate");
		multButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		multButton.setBounds(23, 145, 89, 23);
		contentPane.add(multButton);
		
		divideButton = new JButton("Divide");
		divideButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		divideButton.setBounds(148, 145, 89, 23);
		contentPane.add(divideButton);
		
		modButton = new JButton("Modulo");
		modButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		modButton.setBounds(23, 175, 89, 23);
		contentPane.add(modButton);
		
		derivateButton = new JButton("Derivate");
		derivateButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		derivateButton.setBounds(148, 175, 89, 23);
		contentPane.add(derivateButton);
		
		integrateButton = new JButton("Integrate");
		integrateButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		integrateButton.setBounds(90, 204, 89, 23);
		contentPane.add(integrateButton);

		setVisible(true);
	}

	public String getPol1() {
		return pol1Field.getText();
	}

	public String getPol2() {
		return pol2Field.getText();
	}

	public void setResult(String pol,boolean error){
		resPolField.setText(pol);
		if (error)
			resPolField.setForeground(Color.red);
		else
			resPolField.setForeground(Color.black);

	}

	public void addAddListener(ActionListener al){
		addButton.addActionListener(al);
	}

	public void addSubtractListener(ActionListener al){
		subtractButton.addActionListener(al);
	}

	public void addMultiplyListener(ActionListener al){
		multButton.addActionListener(al);
	}

	public void addDivideListener(ActionListener al){
		divideButton.addActionListener(al);
	}

	public void addModulusListener(ActionListener al){
		modButton.addActionListener(al);
	}

	public void addDerivateListener(ActionListener al){
		derivateButton.addActionListener(al);
	}

	public void addIntegrateListener(ActionListener al){
		integrateButton.addActionListener(al);
	}
}
