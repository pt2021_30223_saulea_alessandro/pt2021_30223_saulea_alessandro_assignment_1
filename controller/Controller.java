package controller;

import model.IntegerPolynomial;
import view.Calculator;

import static model.IntegerPolynomial.stringToPolynomial;

public class Controller {
    public static void main(String[] args){
        Calculator view = new Calculator();
        view.addAddListener(e -> {
            IntegerPolynomial a = stringToPolynomial(view.getPol1());
            IntegerPolynomial b = stringToPolynomial(view.getPol2());
            if (a == null || b == null)
                view.setResult("Invalid polynomials!",true);
            else
                view.setResult(a.add(b).toString(),false);
        });

        view.addSubtractListener(e -> {
            IntegerPolynomial a = stringToPolynomial(view.getPol1());
            IntegerPolynomial b = stringToPolynomial(view.getPol2());
            if (a == null || b == null)
                view.setResult("Invalid polynomials!",true);
            else
                view.setResult(a.subtract(b).toString(),false);
        });

        view.addMultiplyListener(e -> {
            IntegerPolynomial a = stringToPolynomial(view.getPol1());
            IntegerPolynomial b = stringToPolynomial(view.getPol2());
            if (a == null || b == null)
                view.setResult("Invalid polynomials!",true);
            else
                view.setResult(a.multiply(b).toString(),false);
        });

        view.addDivideListener(e -> {
            IntegerPolynomial a = stringToPolynomial(view.getPol1());
            IntegerPolynomial b = stringToPolynomial(view.getPol2());
            if (a == null || b == null)
                view.setResult("Invalid polynomials!",true);
            else
                view.setResult(a.quotient(b).toString(),false);
        });

        view.addModulusListener(e -> {
            IntegerPolynomial a = stringToPolynomial(view.getPol1());
            IntegerPolynomial b = stringToPolynomial(view.getPol2());
            if (a == null || b == null)
                view.setResult("Invalid polynomials!",true);
            else
                view.setResult(a.remainder(b).toString(),false);
        });

        view.addDerivateListener(e -> {
            IntegerPolynomial a = stringToPolynomial(view.getPol1());
            if (a == null)
                view.setResult("Invalid polynomial!",true);
            else
                view.setResult(a.derivate().toString(),false);
        });

        view.addIntegrateListener(e -> {
            IntegerPolynomial a = stringToPolynomial(view.getPol1());
            if (a == null)
                view.setResult("Invalid polynomial!",true);
            else
                view.setResult(a.integral().toString(),false);
        });
    }
}
