package model;

import org.junit.jupiter.api.Test;

import static model.IntegerPolynomial.stringToPolynomial;
import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegerPolynomialTest {

    @Test
    void add() {
        IntegerPolynomial p1 = new IntegerPolynomial();
        p1.getMonomials().add(new IntegerMonomial(1,0));
        p1.getMonomials().add(new IntegerMonomial(3,1));
        p1.getMonomials().add(new IntegerMonomial(2,2));
        IntegerPolynomial p2 = new IntegerPolynomial();
        p2.getMonomials().add(new IntegerMonomial(-5,0));
        p2.getMonomials().add(new IntegerMonomial(6,2));
        p2.getMonomials().add(new IntegerMonomial(1,3));
        IntegerPolynomial s = new IntegerPolynomial();
        s.getMonomials().add(new IntegerMonomial(-4,0));
        s.getMonomials().add(new IntegerMonomial(3,1));
        s.getMonomials().add(new IntegerMonomial(8,2));
        s.getMonomials().add(new IntegerMonomial(1,3));
        assertEquals(p1.add(p2),s);
    }

    @Test
    void subtract() {
        IntegerPolynomial p1 = new IntegerPolynomial();
        p1.getMonomials().add(new IntegerMonomial(1,0));
        p1.getMonomials().add(new IntegerMonomial(3,1));
        p1.getMonomials().add(new IntegerMonomial(2,2));
        IntegerPolynomial p2 = new IntegerPolynomial();
        p2.getMonomials().add(new IntegerMonomial(-5,0));
        p2.getMonomials().add(new IntegerMonomial(6,2));
        p2.getMonomials().add(new IntegerMonomial(1,3));
        IntegerPolynomial s = new IntegerPolynomial();
        s.getMonomials().add(new IntegerMonomial(6,0));
        s.getMonomials().add(new IntegerMonomial(3,1));
        s.getMonomials().add(new IntegerMonomial(-4,2));
        s.getMonomials().add(new IntegerMonomial(-1,3));
        assertEquals(p1.subtract(p2),s);
    }

    @Test
    void multiply() {
        IntegerPolynomial p1 = new IntegerPolynomial();
        p1.getMonomials().add(new IntegerMonomial(1,0));
        p1.getMonomials().add(new IntegerMonomial(-2,1));
        p1.getMonomials().add(new IntegerMonomial(1,2));
        IntegerPolynomial p2 = new IntegerPolynomial();
        p2.getMonomials().add(new IntegerMonomial(1,0));
        p2.getMonomials().add(new IntegerMonomial(1,1));
        IntegerPolynomial pr = new IntegerPolynomial();
        pr.getMonomials().add(new IntegerMonomial(1,0));
        pr.getMonomials().add(new IntegerMonomial(-1,1));
        pr.getMonomials().add(new IntegerMonomial(-1,2));
        pr.getMonomials().add(new IntegerMonomial(1,3));
        assertEquals(p1.multiply(p2),pr);

    }

    @Test
    void derivate() {
        IntegerPolynomial p = new IntegerPolynomial();
        p.getMonomials().add(new IntegerMonomial(-5,0));
        p.getMonomials().add(new IntegerMonomial(6,1));
        p.getMonomials().add(new IntegerMonomial(-2,2));
        p.getMonomials().add(new IntegerMonomial(1,3));
        IntegerPolynomial pd = new IntegerPolynomial();
        pd.getMonomials().add(new IntegerMonomial(6,0));
        pd.getMonomials().add(new IntegerMonomial(-4,1));
        pd.getMonomials().add(new IntegerMonomial(3,2));
        assertEquals(p.derivate(),pd);
    }

    @Test
    void integral() {
        IntegerPolynomial p = new IntegerPolynomial();
        p.getMonomials().add(new IntegerMonomial(5,0));
        p.getMonomials().add(new IntegerMonomial(4,2));
        p.getMonomials().add(new IntegerMonomial(1,3));
        RealPolynomial pi = new RealPolynomial();
        pi.getMonomials().add(new RealMonomial(5.0,1));
        pi.getMonomials().add(new RealMonomial(4.0/3,3));
        pi.getMonomials().add(new RealMonomial(1.0/4,4));
        assertEquals(p.integral(),pi);
    }

    @Test
    void quotient() {
        IntegerPolynomial p = new IntegerPolynomial();
        p.getMonomials().add(new IntegerMonomial(-5,0));
        p.getMonomials().add(new IntegerMonomial(6,1));
        p.getMonomials().add(new IntegerMonomial(-2,2));
        p.getMonomials().add(new IntegerMonomial(1,3));
        IntegerPolynomial q = new IntegerPolynomial();
        q.getMonomials().add(new IntegerMonomial(-1,0));
        q.getMonomials().add(new IntegerMonomial(1,2));
        RealPolynomial qu = new RealPolynomial();
        qu.getMonomials().add(new RealMonomial(-2,0));
        qu.getMonomials().add(new RealMonomial(1,1));
        assertEquals(p.quotient(q),qu);
    }

    @Test
    void remainder() {
        IntegerPolynomial p = new IntegerPolynomial();
        p.getMonomials().add(new IntegerMonomial(-5,0));
        p.getMonomials().add(new IntegerMonomial(6,1));
        p.getMonomials().add(new IntegerMonomial(-2,2));
        p.getMonomials().add(new IntegerMonomial(1,3));
        IntegerPolynomial q = new IntegerPolynomial();
        q.getMonomials().add(new IntegerMonomial(-1,0));
        q.getMonomials().add(new IntegerMonomial(1,2));
        RealPolynomial rem = new RealPolynomial();
        rem.getMonomials().add(new RealMonomial(-7,0));
        rem.getMonomials().add(new RealMonomial(7,1));
        assertEquals(p.remainder(q),rem);
    }

    @Test
    void testStringToPolynomial() {
        IntegerPolynomial p = new IntegerPolynomial();
        p.getMonomials().add(new IntegerMonomial(5,0));
        p.getMonomials().add(new IntegerMonomial(1,2));
        assertEquals(stringToPolynomial("2-5x+x^2+3+5x+0x^3"),p);
    }
}