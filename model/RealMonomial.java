package model;

public class RealMonomial implements Comparable<RealMonomial>{
    private final double coefficient;
    private final int grade;

    public RealMonomial(double coefficient, int grade) {
        this.coefficient = coefficient;
        this.grade = grade;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public int getGrade() {
        return grade;
    }
    
    public RealMonomial subtract(RealMonomial im){
        return new RealMonomial(coefficient-im.getCoefficient(),grade);
    }

    public RealMonomial negate(){
        return new RealMonomial(-coefficient,grade);
    }

    public RealMonomial multiply(RealMonomial p){
        return new RealMonomial(coefficient*p.getCoefficient(),grade+p.getGrade());
    }
    
    @Override
    public int compareTo(RealMonomial o) {
        return grade-o.getGrade();
    }

    @Override
    public String toString() {
        String str = "";

        if (getGrade()==0)
            str = String.format("%.2f",coefficient); // afisez coeficientul cu 2 zecimale
        else {
            if (coefficient == -1)
                str = str +"-";
            else if (coefficient !=1)
                str = String.format("%.2f",coefficient);
            str = str + "x";
            if (getGrade() > 1)
                str = str + "^" + getGrade();
        }
        return str;
    }

    @Override
    public boolean equals(Object o) { // generata automat
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RealMonomial that = (RealMonomial) o;
        return Double.compare(that.coefficient, coefficient) == 0 && grade == that.grade;
    }
}
