package model;

public class IntegerMonomial implements Comparable<IntegerMonomial>{
    private final int coefficient;
    private final int grade;

    public IntegerMonomial(int coefficient, int grade) {
        this.coefficient = coefficient;
        this.grade = grade;
    }

    public int getCoefficient() {
        return coefficient;
    }

    public int getGrade() {
        return grade;
    }

    @Override
    public int compareTo(IntegerMonomial o) {
        return grade-o.getGrade();
    } // compar doua monoame dupa grad

    public IntegerMonomial add(IntegerMonomial im){
        return new IntegerMonomial(coefficient+im.getCoefficient(),grade);
    }

    public IntegerMonomial subtract(IntegerMonomial im){
        return new IntegerMonomial(coefficient-im.getCoefficient(),grade);
    }

    public IntegerMonomial negate(){
        return new IntegerMonomial(-coefficient,grade);
    }

    public IntegerMonomial multiply(IntegerMonomial p){
        return new IntegerMonomial(coefficient*p.getCoefficient(),grade+p.getGrade());
    }

    public IntegerMonomial derivate(){ // (x^n)'= n*x^(n-1)
        if (grade==0)
            return new IntegerMonomial(0,0);
        return new IntegerMonomial(grade*coefficient,grade-1);
    }

    public RealMonomial integral(){ // I(x^n) = x^(n+1)/(n+1)
        return new RealMonomial((double)coefficient/(grade+1),grade+1);
    }

    public RealMonomial toRealMonomial(){
        return new RealMonomial(coefficient,grade);
    }

    @Override
    public String toString() {
        String str = "";
        if (getGrade()==0)
            str = String.valueOf(coefficient);
        else {
            if (coefficient == -1)
                str = str +"-";
            else if (coefficient !=1)
                str = String.valueOf(coefficient);
            str = str + "x";
            if (getGrade() > 1)
                str = str + "^" + getGrade();
        }
        return str;
    }

    @Override
    public boolean equals(Object o) { // generata automat, si chiar e corecta
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerMonomial that = (IntegerMonomial) o;
        return coefficient == that.coefficient && grade == that.grade;
    }
}
