package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class RealPolynomial {
    private final ArrayList<RealMonomial> monomials;

    public RealPolynomial() {
        this.monomials = new ArrayList<>();
    }

    public ArrayList<RealMonomial> getMonomials() {
        return monomials;
    }

    public int getGrade(){
        int maxGrade = 0;
        for (RealMonomial rm : monomials)
            maxGrade = Math.max(maxGrade,rm.getGrade());
        return maxGrade;
    }

    public RealPolynomial subtract(RealPolynomial p){ // la fel ca la scaderea polinoamelor cu coeficienti intregi
        Collections.sort(this.getMonomials());
        Collections.sort(p.getMonomials());
        int i=0, j=0;
        RealPolynomial s = new RealPolynomial();
        while(i<this.getMonomials().size() && j<p.getMonomials().size()){
            RealMonomial rm1 = this.getMonomials().get(i);
            RealMonomial rm2 = p.getMonomials().get(j);
            if (rm1.getGrade() == rm2.getGrade()){
                if (!rm1.equals(rm2))
                    s.getMonomials().add(rm1.subtract(rm2));
                ++i; ++j;
            }
            else if (rm1.getGrade()<rm2.getGrade()){
                s.getMonomials().add(rm1);
                ++i;
            }
            else{
                s.getMonomials().add(rm2.negate());
                ++j;
            }
        }
        while(i<this.getMonomials().size()) {
            s.getMonomials().add(this.getMonomials().get(i));
            ++i;
        }
        while(j<p.getMonomials().size()) {
            s.getMonomials().add(p.getMonomials().get(j).negate());
            ++j;
        }
        return s;
    }
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        if (monomials.isEmpty()) // polinom nul
            str.append("0");
        else for (RealMonomial im : monomials)
            if (im.getCoefficient()>=0)
                str.append("+").append(im);
            else
                str.append(im);
        return str.toString();
    }

    @Override
    public boolean equals(Object o) { // generata automat
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RealPolynomial that = (RealPolynomial) o;
        return Objects.equals(monomials, that.monomials);
    }
}
