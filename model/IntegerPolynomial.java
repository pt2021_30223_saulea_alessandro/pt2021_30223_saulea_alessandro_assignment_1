package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IntegerPolynomial {
    private final ArrayList<IntegerMonomial> monomials;

    public IntegerPolynomial() {
        monomials = new ArrayList<>();
    }

    public ArrayList<IntegerMonomial> getMonomials() {
        return monomials;
    }

    public IntegerPolynomial add(IntegerPolynomial p){
        // sortez monoamele polinoamelor crescator dupa grad, pentru a face mai usoara adunarea coeficientilor de acelasi grad
        Collections.sort(this.getMonomials());
        Collections.sort(p.getMonomials());
        int i=0, j=0;
        // aplic un algoritm asemanator interclasarii a 2 vectori sortati
        IntegerPolynomial s = new IntegerPolynomial();
        while(i<this.getMonomials().size() && j<p.getMonomials().size()){
            IntegerMonomial im1 = this.getMonomials().get(i);
            IntegerMonomial im2 = p.getMonomials().get(j);
            if (im1.getGrade() == im2.getGrade()){ // daca monoamele au acelasi grad, le adun
                if (!im1.equals(im2.negate()))
                    s.getMonomials().add(im1.add(im2));
                ++i; ++j;
            }
            // altfel adaug cate un monom pe rand in suma celor 2 polinoame
            else if (im1.getGrade()<im2.getGrade()){
                s.getMonomials().add(im1);
                ++i;
            }
            else{
                s.getMonomials().add(im2);
                ++j;
            }
        }
        while(i<this.getMonomials().size()) {
            s.getMonomials().add(this.getMonomials().get(i));
            ++i;
        }
        while(j<p.getMonomials().size()) {
            s.getMonomials().add(p.getMonomials().get(j));
            ++j;
        }
        return s;
    }

    public IntegerPolynomial subtract(IntegerPolynomial p){
        // analog ca la adunare
        Collections.sort(this.getMonomials());
        Collections.sort(p.getMonomials());
        int i=0, j=0;
        IntegerPolynomial s = new IntegerPolynomial();
        while(i<this.getMonomials().size() && j<p.getMonomials().size()){
            IntegerMonomial im1 = this.getMonomials().get(i);
            IntegerMonomial im2 = p.getMonomials().get(j);
            if (im1.getGrade() == im2.getGrade()){
                if (!im1.equals(im2))
                    s.getMonomials().add(im1.subtract(im2));
                ++i; ++j;
            }
            else if (im1.getGrade()<im2.getGrade()){
                s.getMonomials().add(im1);
                ++i;
            }
            else{
                s.getMonomials().add(im2.negate());
                ++j;
            }
        }
        while(i<this.getMonomials().size()) {
            s.getMonomials().add(this.getMonomials().get(i));
            ++i;
        }
        while(j<p.getMonomials().size()) {
            s.getMonomials().add(p.getMonomials().get(j).negate());
            ++j;
        }
        return s;
    }

    public IntegerPolynomial multiply(IntegerPolynomial p){
        // inmultirea a 2 polinoame se face prin inmultirea fiecarui polinom din primul polinom cu fiecare polinom din al doilea
        // si adunarea monoamelor obtinute
        IntegerPolynomial pr = new IntegerPolynomial(); // adaug monomul cu coef 0 si de grad 0 ca sa pot face adunarea pe acest polinom
        for (IntegerMonomial im : p.getMonomials()){
            IntegerPolynomial p1 = new IntegerPolynomial();
            for (IntegerMonomial im1 : this.getMonomials())
                p1.getMonomials().add(im1.multiply(im));
            pr=pr.add(p1);
        }
        return pr;
    }

    public IntegerPolynomial derivate(){ // derivarea unui polinom se face prin derivarea fiecarui monom in parte
        IntegerPolynomial pd = new IntegerPolynomial();
        for (IntegerMonomial im : monomials)
            if (im.getGrade()>0)
                pd.getMonomials().add(im.derivate());
        if (pd.getMonomials().isEmpty())
            pd.getMonomials().add(new IntegerMonomial(0,0));
        return pd;
    }

    public RealPolynomial integral(){ // integrarea unui polinom se face prin integrarea fiecarui monom in parte
        RealPolynomial pi = new RealPolynomial();
        for (IntegerMonomial im : monomials)
            pi.getMonomials().add(im.integral());
        return pi;
    }

    public RealPolynomial toRealPolynomial(){
        RealPolynomial pr = new RealPolynomial();
        for (IntegerMonomial im : monomials)
            pr.getMonomials().add(im.toRealMonomial());
        return pr;
    }

    public RealPolynomial quotient(IntegerPolynomial divisor){
        // am convertit deimpartitul si impartitorul la polinoame cu coeficienti reali, pentru ca rezultatele sa iasa cum trebuie, cu virgula
        RealPolynomial rem = this.toRealPolynomial(); // acesta va fi restul impartirii dupa ce s-a efectuat impartirea
        RealPolynomial q = divisor.toRealPolynomial(); // impartitorul
        RealPolynomial qu = new RealPolynomial(); // catul
        while(rem.getGrade()>=q.getGrade()){
            // sortez monoamele polinoamelor in ordine descrescatoare dupa grad
            rem.getMonomials().sort(Collections.reverseOrder());
            q.getMonomials().sort(Collections.reverseOrder());
            RealMonomial rm = new RealMonomial(
                    rem.getMonomials().get(0).getCoefficient()/q.getMonomials().get(0).getCoefficient(),
                             rem.getGrade()-q.getGrade());
            qu.getMonomials().add(rm);
            RealPolynomial p1 = new RealPolynomial();
            for (RealMonomial rm1 : q.getMonomials())
                p1.getMonomials().add(rm1.multiply(rm));
            rem=rem.subtract(p1);
        }
        Collections.sort(qu.getMonomials());
        return qu;
    }

    public RealPolynomial remainder(IntegerPolynomial divisor){ // acelasi algoritm ca si la quotient()
        RealPolynomial rem = this.toRealPolynomial();
        RealPolynomial q = divisor.toRealPolynomial();
        RealPolynomial qu = new RealPolynomial();
        while(rem.getGrade()>=q.getGrade()){
            rem.getMonomials().sort(Collections.reverseOrder());
            q.getMonomials().sort(Collections.reverseOrder());
            RealMonomial rm = new RealMonomial(
                    rem.getMonomials().get(0).getCoefficient()/q.getMonomials().get(0).getCoefficient(),
                    rem.getGrade()-q.getGrade());
            qu.getMonomials().add(rm);
            RealPolynomial p1 = new RealPolynomial();
            for (RealMonomial rm1 : q.getMonomials())
                p1.getMonomials().add(rm1.multiply(rm));
            rem=rem.subtract(p1);
        }
        Collections.sort(rem.getMonomials());
        return rem;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        if (monomials.isEmpty())
            str.append("0");
        else for (IntegerMonomial im : monomials)
            if (im.getCoefficient()>=0)
                str.append("+").append(im);
            else
                str.append(im);
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {  //generata automat
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerPolynomial that = (IntegerPolynomial) o;
        return monomials.equals(that.monomials);
    }

    public static IntegerPolynomial stringToPolynomial(String str){
        // verific mai intai daca stringul str respecta structura unui polinom
        String monomial_pattern = "(\\d*x(\\^\\d+)?|\\d+)";
        Pattern pattern = Pattern.compile("[+-]?"+monomial_pattern+"([+-]"+monomial_pattern+")*");
        Matcher matcher = pattern.matcher(str);
        if (!matcher.matches()) // daca nu respecta atunci ma opresc
            return null;
        //altfel voi extrage fiecare monom din stringul str folosind pattern-ul de mai jos
        IntegerPolynomial p = new IntegerPolynomial();
        pattern = Pattern.compile("[+-]*\\d*x(\\^\\d+)?|[+-]*\\d+");
        matcher = pattern.matcher(str);
        while(matcher.find()){
            String monomial = matcher.group();
            int coefficient,grade;
            if (!monomial.contains("x")){ // daca monomul nu contine 'x' atunci gradul sau va fi 0
                coefficient = Integer.parseInt(monomial);
                if (coefficient == 0) // daca coeficientul este 0, trec la urmatorul monom
                    continue;
                grade = 0;
            }
            else if (monomial.startsWith("x")) { // daca monomul incepe cu 'x'
                coefficient = 1;
                String[] parts = monomial.split("x"); //parts[0] = coeficientul, parts[1]=gradul
                if (parts.length == 0)
                    grade = 1;
                else
                    grade = Integer.parseInt(parts[1].substring(1));

            }
            else{
                String[] parts = monomial.split("x");
                if (parts[0].equals("+")) // daca avem doar '+' inainte de 'x'
                    coefficient = 1;
                else if (parts[0].equals("-")) // daca avem doar '-' inainte d 'x'
                    coefficient = -1;
                else
                    coefficient = Integer.parseInt(parts[0]);
                if (coefficient == 0)
                    continue;
                if (parts.length == 1)
                    grade = 1;
                else
                    grade = Integer.parseInt(parts[1].substring(1));
            }
            IntegerMonomial im = new IntegerMonomial(coefficient,grade);
            boolean found = false;
            for (int i=0;i<p.getMonomials().size();++i) {
                IntegerMonomial im1 = p.getMonomials().get(i);
                if (im1.getGrade() == grade) {
                    p.getMonomials().set(i,im1.add(im));
                    found = true;
                    break;
                }
            }
            if (!found)
                p.getMonomials().add(im);
        }
        p.getMonomials().removeIf(im -> im.getCoefficient() == 0);
        Collections.sort(p.getMonomials());
        return p;
    }
}
